package validator

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

var ErrNotStruct = errors.New("wrong argument given, should be a struct")
var ErrInvalidValidatorSyntax = errors.New("invalid validator syntax")
var ErrValidateForUnexportedFields = errors.New("validation for unexported field is not allowed")
var ErrInvalidFieldType = errors.New("unsupported field type")

type ValidationError struct {
	Err error
}

type ValidationErrors []ValidationError

func (v ValidationErrors) Error() string {
	errs := make([]error, len(v))

	for i, err := range v {
		errs[i] = err.Err
	}

	return errors.Join(errs...).Error()
}

func Validate(v any) error {
	dt := reflect.TypeOf(v)

	if !(dt.Kind() == reflect.Struct) {
		return ErrNotStruct
	}

	validationErrors := ValidationErrors{}

	for i := 0; i < dt.NumField(); i++ {
		f := dt.Field(i)

		if f.Type.Kind() == reflect.Struct {
			if err := Validate(reflect.ValueOf(v).Field(i).Interface()); err != nil {
				validationErrors = append(validationErrors, err.(ValidationErrors)...)
			}
		}

		tagValue := f.Tag.Get("validate")

		if tagValue == "" {
			continue
		}

		rules := strings.Split(tagValue, ";")

		for _, rule := range rules {
			if !f.IsExported() {
				validationErrors = append(validationErrors, ValidationError{
					Err: ErrValidateForUnexportedFields,
				})
				continue
			}
	
			name, value, err := parseValidator(rule)
	
			if err != nil {
				validationErrors = append(validationErrors, ValidationError{
					Err: err,
				})
				continue
			}
	
			if f.Type.Kind() == reflect.Int {
				if err := validateInt(name, value, f.Name, int(reflect.ValueOf(v).Field(i).Int())); err != nil {
					validationErrors = append(validationErrors, ValidationError{Err: err})
				}
			} else if f.Type.Kind() == reflect.String {
				if err := validateString(name, value, f.Name, reflect.ValueOf(v).Field(i).String()); err != nil {
					validationErrors = append(validationErrors, ValidationError{Err: err})
				}
			} else {
				validationErrors = append(validationErrors, ValidationError{
					Err: ErrInvalidFieldType,
				})
			}
		}
	}

	if len(validationErrors) > 0 {
		return validationErrors
	}

	return nil
}

func parseValidator(vr string) (string, string, error) {
	tag := strings.Split(vr, ":")

	if len(tag) != 2 {
		return "", "", ErrInvalidValidatorSyntax
	}

	return tag[0], tag[1], nil
}

func validateInt(validatorName, validatorValue, field string, value int) error {
	switch validatorName {
	case "in":
		in := strings.Split(validatorValue, ",")
		if len(in) == 0 {
			return ErrInvalidValidatorSyntax
		}

		nums := make([]int, len(in))
		for i, v := range in {
			n, err := strconv.Atoi(v)
			if err != nil {
				return ErrInvalidValidatorSyntax
			}
			nums[i] = n
		}

		if err := validateIn(nums, value); err != nil {
			return fmt.Errorf("%s: %w", field, err)
		}
	case "min":
		min, err := strconv.Atoi(validatorValue)
		if err != nil {
			return ErrInvalidValidatorSyntax
		}

		if err := validateMin(min, value); err != nil {
			return fmt.Errorf("%s: %w", field, err)
		}
	case "max":
		max, err := strconv.Atoi(validatorValue)
		if err != nil {
			return ErrInvalidValidatorSyntax
		}

		if err := validateMax(max, value); err != nil {
			return fmt.Errorf("%s: %w", field, err)
		}
	default:
		return ErrInvalidValidatorSyntax
	}

	return nil
}

func validateString(validatorName, validatorValue, field string, value string) error {
	switch validatorName {
	case "len":
		l, err := strconv.Atoi(validatorValue)
		if err != nil {
			return ErrInvalidValidatorSyntax
		}

		if err := validateLen(l, value); err != nil {
			return fmt.Errorf("%s: %w", field, err)
		}
	case "in":
		in := strings.Split(validatorValue, ",")
		if len(in) == 0 || len(in) == 1 && in[0] == "" {
			return ErrInvalidValidatorSyntax
		}

		if err := validateIn(in, value); err != nil {
			return fmt.Errorf("%s: %w", field, err)
		}
	case "min":
		min, err := strconv.Atoi(validatorValue)
		if err != nil {
			return ErrInvalidValidatorSyntax
		}

		if err := validateMin(min, len(value)); err != nil {
			return fmt.Errorf("%s: %w", field, err)
		}
	case "max":
		max, err := strconv.Atoi(validatorValue)
		if err != nil {
			return ErrInvalidValidatorSyntax
		}

		if err := validateMax(max, len(value)); err != nil {
			return fmt.Errorf("%s: %w", field, err)
		}
	default:
		return ErrInvalidValidatorSyntax
	}

	return nil
}

func validateIn[T comparable](in []T, f T) error {
	for _, v := range in {
		if v == f {
			return nil
		}
	}

	return errors.New("value is not in the list")
}

func validateMin(min int, value int) error {
	if value < min {
		return errors.New("value is less than min")
	}

	return nil
}

func validateMax(max int, value int) error {
	if value > max {
		return errors.New("value is more than max")
	}

	return nil
}

func validateLen(l int, f string) error {
	if len(f) != l {
		return errors.New("invalid length")
	}

	return nil
}
